package test.java;

import model.Client;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class ClientTest {
	
	@Test
	public void getCodeTestEntier() {
		Client c = new Client(2, "hayri", "Hayri@gmail.com", 2);
		assertEquals(2, c.getCode());
	}	

}
